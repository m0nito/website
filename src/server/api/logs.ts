import express from 'express'
import { format } from '@fast-csv/format'

import { Logs } from '../db'
import { accessMiddleware } from '../accessMiddleware'

const logRouter = express.Router()

logRouter.post('/logs', accessMiddleware('moderator'), async (req, res) => {
  const { startDate, endDate, type, vps, app } = req.body

  const filter = {
    timestamp: {
      $gte: startDate,
      $lte: endDate,
    },
    ...(!!type && { type }),
    ...(!!vps && { vps }),
    ...(!!app && { app }),
  }

  const cursor = Logs.find(filter).cursor()

  res.setHeader('Content-disposition', `attachment; filename=exports.csv`)
  res.writeHead(200, { 'Content-Type': 'text/csv' })

  res.flushHeaders()

  const csvStream = format({ headers: true }).transform((doc) => ({
    Timestamp: doc.timestamp.getTime(),
    Type: doc.type,
    Value: doc.value,
    App: doc.app,
    Vps: doc.vps,
  }))

  cursor.pipe(csvStream).pipe(res)
})

logRouter.get('/purge', accessMiddleware('admin'), (_req, res) => {
  Logs.deleteMany({}, (err) => {
    if (err) {
      res.status(500).end()
    } else {
      res.end()
    }
  })
})

export default logRouter
