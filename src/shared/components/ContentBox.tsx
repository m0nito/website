import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Paper } from '@material-ui/core'

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    margin: theme.spacing(3),
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
  },
}))

export const ContentBox: React.FC = ({ ...props }) => {
  const classes = useStyles()

  return <Paper {...props} variant="outlined" className={classes.root} />
}
