import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#7335d4',
    },
    secondary: {
      main: '#1976d2',
    },
  },
})

export default theme
