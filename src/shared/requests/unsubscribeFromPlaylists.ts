import axios from 'axios'

export interface unsubscribeFromPlaylistsProps {
  token: string;
  onFinish?: (successCounter: number, counter: number) => void;
  onFailure?: () => void;
  onActionFailure?: (msg: string) => void;
  isAlternateEndpoint?: boolean;
}

export default async function unsubscribeFromPlaylists({
  token,
  onFinish = null,
  onFailure = null,
  onActionFailure = null,
  isAlternateEndpoint = false,
}: unsubscribeFromPlaylistsProps): void {
  const playlists = await axios.get(
    `api/playlists/unsubscribe${isAlternateEndpoint ? '/reserve' : ''}`
  )

  const options = {
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json',
    },
  }

  const actions = playlists.data.map(({ id, name, ...rest }) => {
    const playlist_uri = rest['playlist uri']
    const errMessage = `Playlist '${name}' does't exist on user acc`

    return new Promise(async (resolve, reject) => {
      try {
        if (playlist_uri) {
          const playlist_id = playlist_uri.slice(17)

          const spUserPlData = await axios.get(
            `https://api.spotify.com/v1/me/playlists`,
            options
          )

          const spUserPlArr = spUserPlData.data.items

          if (
            spUserPlArr.length > 0 &&
            spUserPlArr.find((pl) => pl.id === playlist_id)
          ) {
            await axios.delete(
              `https://api.spotify.com/v1/playlists/${playlist_id}/followers`,
              options
            )
            resolve({
              id,
              fields: {
                unsubscribed: true,
              },
            })
          } else {
            onActionFailure(errMessage)
            reject(errMessage)
          }
        } else {
          onActionFailure(errMessage)
          reject(errMessage)
        }
      } catch (e) {
        reject('Network error')
      }
    })
  })

  try {
    const playlistResponce = await Promise.allSettled(actions)

    const playlistReturnRecords = playlistResponce
      .filter((action) => action.status === 'fulfilled')
      .map((action) => action.value)

    await axios.post(
      `api/playlists/send${isAlternateEndpoint ? '/reserve' : ''}`,
      {
        playlists: playlistReturnRecords,
      }
    )

    onFinish(playlistReturnRecords.length, playlistResponce.length)
  } catch (e) {
    onFailure()
  }
}
