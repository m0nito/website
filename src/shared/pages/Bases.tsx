import React from 'react'

import { Head } from '../components/Head'
import { BasesTable } from '../components/BasesTable'

const Bases: React.FC = () => (
  <>
    <Head title="Bases Control panel" description="Bases Control panel" />
    <BasesTable />
  </>
)

export default Bases
