import express from 'express'

import { Settings } from '../db'

const settingsRouter = express.Router()

settingsRouter.get('/', async (req, res) => {
  try {
    const settings = await Settings.find({}).lean()
    res.send(settings[0])
  } catch (e) {
    res.status(500).end(e)
  }
})

settingsRouter.post('/', async (req, res) => {
  try {
    const { _id, airtable, webhook, fileLimit } = req.body

    const settings = await Settings.findByIdAndUpdate(_id, {
      airtable,
      webhook,
      fileLimit: Number(fileLimit),
    }).lean()

    res.send(settings)
  } catch (e) {
    res.status(500).end(e)
  }
})

export default settingsRouter
