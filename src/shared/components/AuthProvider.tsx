import React from 'react'

// eslint-disable-next-line
const noop = () => {}

export interface AuthProviderProps {
  roleProp?: string;
}

export const AuthContext = React.createContext({
  role: 'unauthorized',
  setRole: noop,
})

export const AuthProvider: React.FC = ({
  roleProp = 'unauthorized',
  children,
}: AuthProviderProps) => {
  const [role, setRole] = React.useState(roleProp)

  const context = React.useMemo(() => {
    return {
      role,
      setRole,
    }
  }, [role])

  return <AuthContext.Provider value={context}>{children}</AuthContext.Provider>
}
