import React, { useState } from 'react'
import { navigate } from '@reach/router'
import axios from 'axios'
import { useSnackbar } from 'notistack'
import { makeStyles } from '@material-ui/core/styles'
import {
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
  IconButton,
  Button,
  Typography,
  Toolbar,
} from '@material-ui/core'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'

import { Head } from '../components/Head'
import { ContentBox } from '../components/ContentBox'

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    padding: theme.spacing(1),
  },
  container: {
    width: '32ch',
    padding: theme.spacing(1),
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  form__field: {
    marginBottom: theme.spacing(1),
    width: '100%'
  },
  form__submit: {
    marginTop: theme.spacing(2),
  },
}))

const defaultValue = {
  email: '',
  role: 'admin',
  password: '',
  repeat: '',
  showPassword: false,
}

const NewUser: React.FC = () => {

  const classes = useStyles()
  const { enqueueSnackbar } = useSnackbar()
  const [values, setValues] = useState<State>(defaultValue)

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword })
  }

   // eslint-disable-next-line
   const handleChange = (prop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [prop]: event.target.value });
  };
  
  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
  };


  const handleSubmit = async (event: React.SubmitEvent<HTMLButtonElement>) => {

    event.preventDefault()

    try{
      await axios.post('api/users/create', { 
        email: values.email, 
        password: values.password,
        role: values.role, 
        repeat: values.repeat
      })
      enqueueSnackbar('Successfully create new user',{ 
        variant: 'success',
      });
      navigate('users')
    }catch(e){
      enqueueSnackbar('Can`t create new user',{ 
        variant: 'error',
      });
    }
  }

  return (
    <>
      <Head title="Create new user" description="Create new user" />
      <ContentBox>
        <Toolbar disableGutters>
        <Typography className={classes.title} variant="h5">New user</Typography>
        </Toolbar>
        <div className={classes.container}>
        <form className={classes.form} onSubmit={handleSubmit} noValidate autoComplete="off">
          <FormControl className={classes.form__field}>
            <InputLabel shrink>email</InputLabel>
            <Input 
              name="email" 
              type="email"
              value={values.email}
              onChange={handleChange('email')}
            />
          </FormControl>
          <FormControl className={classes.form__field}>
            <InputLabel shrink>role</InputLabel>
            <Select name="role" value={values.role} onChange={handleChange('role')}>
              <MenuItem value={'admin'}>admin</MenuItem>
              <MenuItem value={'moderator'}>moderator</MenuItem>
            </Select>
          </FormControl>
          <FormControl className={classes.form__field}>
            <InputLabel shrink>password</InputLabel>
            <Input
              name="password"
              type={values.showPassword ? 'text' : 'password'}
              value={values.password}
              onChange={handleChange('password')}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
          <FormControl className={classes.form__field}>
            <InputLabel shrink>repeat password</InputLabel>
            <Input
              name="repeat"
              type={values.showPassword ? 'text' : 'password'}
              value={values.repeat}
              onChange={handleChange('repeat')}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
          <Button
            className={classes.form__submit}
            type="submit"
            variant="contained"
            color="primary"
          >
            Create
          </Button>
        </form>
        </div>
      </ContentBox>
    </>
  )
}

export default NewUser
