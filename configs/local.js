module.exports = {
  basepath: '/',
  port: 8000,
  dbHost: 'mongodb://localhost/test',
  webhook: 'http://localhost:8001/v2/webhook',
  dbConfig: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  },
  uploadFolder: 'uploads',
}
