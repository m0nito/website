import express from 'express'
import bodyParser from 'body-parser'

import { accessMiddleware } from '../accessMiddleware'

import fileRouter from './files'
import webhookRouter from './webhook'
import authRouter from './auth'
import vpsRouter from './vps'
import playlistRouter from './playlists'
import userRouter from './users'
import logRouter from './logs'
import settingsRouter from './settings'
import baseRouter from './bases'

const api = express.Router()

api.use(bodyParser.json({ limit: '10mb' }))

api.use('/webhook', webhookRouter)
api.use(authRouter)
api.use('/vps', accessMiddleware('moderator'), vpsRouter)
api.use('/playlists', playlistRouter)
api.use('/users', accessMiddleware('admin'), userRouter)
api.use(logRouter)
api.use('/settings', accessMiddleware('admin'), settingsRouter)
api.use('/files', accessMiddleware('moderator'), fileRouter)
api.use('/bases', accessMiddleware('admin'), baseRouter)

export default api
