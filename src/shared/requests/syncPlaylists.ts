import axios from 'axios'

export interface syncPlaylistsProps {
  token: string;
  onFinish?: (successCounter: number, counter: number) => void;
  onFailure?: () => void;
  onActionFailure?: (playlistId: string) => void;
  isAlternateEndpoint?: boolean;
}

export default async function syncPlayLists({
  token,
  onFinish = null,
  onFailure = null,
  onActionFailure = null,
  isAlternateEndpoint = false,
}: syncPlaylistsProps): void {
  const selections = await axios.get(
    `api/playlists${isAlternateEndpoint ? '/reserve' : ''}`
  )

  const user = await axios.get('https://api.spotify.com/v1/me', {
    headers: {
      Authorization: 'Bearer ' + token,
    },
  })

  const options = {
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json',
    },
  }

  const actions = selections.data.map(
    ({ id, name, description, uris, ...rest }) => {
      const playlist_uri = rest['playlist uri']

      return new Promise(async (resolve, reject) => {
        try {
          if (!playlist_uri) {
            const playlist = await axios.post(
              `https://api.spotify.com/v1/users/${user.data.id}/playlists`,
              { name, description },
              options
            )

            await axios.put(
              `https://api.spotify.com/v1/playlists/${playlist.data.id}/tracks`,
              { uris },
              options
            )

            resolve({
              id,
              fields: {
                'playlist uri': playlist.data.uri,
                unsubscribed: false,
              },
            })
          } else {
            const playlist_id = playlist_uri.slice(17)

            const spUserPlData = await axios.get(
              `https://api.spotify.com/v1/me/playlists`,
              options
            )

            const spUserPlArr = spUserPlData.data.items

            if (
              spUserPlArr.length > 0 &&
              spUserPlArr.find((pl) => pl.id === playlist_id)
            ) {
              await axios.put(
                `https://api.spotify.com/v1/playlists/${playlist_uri.slice(
                  17
                )}/tracks`,
                { uris },
                options
              )

              resolve({
                id,
                fields: {
                  'playlist uri': playlist_uri,
                  unsubscribed: false,
                },
              })
            } else {
              onActionFailure(playlist_id)
              reject(`playlist ${playlist_id} does't exist on user acc`)
            }
          }
        } catch (e) {
          reject('Network error')
        }
      })
    }
  )

  try {
    const playlistResponce = await Promise.allSettled(actions)

    const playlistReturnRecords = playlistResponce
      .filter((action) => action.status === 'fulfilled')
      .map((action) => action.value)

    await axios.post(
      `api/playlists/send${isAlternateEndpoint ? '/reserve' : ''}`,
      {
        playlists: playlistReturnRecords,
      }
    )

    onFinish(playlistReturnRecords.length, playlistResponce.length)
  } catch (e) {
    onFailure()
  }
}
