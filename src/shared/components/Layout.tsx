import React from 'react'
import axios from 'axios'
import { useSnackbar } from 'notistack'
import { makeStyles, Theme } from '@material-ui/core/styles'
import { Link as RouterLink } from '@reach/router'
import {
  Typography,
  Drawer,
  Toolbar,
  Button,
  AppBar,
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core'

import { AuthContext } from './AuthProvider'

const drawerWidth = 240

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: 'flex',
  },
  title: {
    marginLeft: theme.spacing(2),
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
  },
  header: {
    backgroundColor: theme.palette.background.default,
  },
  menuButton: {
    marginLeft: theme.spacing(2),
  },
  listNavLink: {
    color: theme.palette.text.primary,
    '&:hover': {
      backgroundColor: 'inherit',
    },
    '&[aria-current="page"]': {
      '& div': {
        pointerEvents: 'none',
        color: theme.palette.primary.main,
      },
    },
  },
}))

interface ListNavLinkProps {
  text: string;
}

function ListNavLink({ text, ...props }: ListNavLinkProps) {
  const classes = useStyles()
  return (
    <ListItem
      button
      disableRipple
      {...props}
      className={classes.listNavLink}
      component={RouterLink}
    >
      <ListItemText primary={text} />
    </ListItem>
  )
}

interface LayoutProps {
  children?: React.ReactChildren;
}

export default function Layout({ children }: LayoutProps): React.FC {
  const classes = useStyles()

  const { enqueueSnackbar } = useSnackbar()

  const { role, setRole } = React.useContext(AuthContext)

  const handleLogout = async () => {
    try {
      await axios.get('api/logout')
      enqueueSnackbar('Successfully logout', {
        variant: 'success',
      })
      setRole('unauthorized')
    } catch (e) {
      enqueueSnackbar('Internal server error', {
        variant: 'error',
      })
    }
  }

  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
      >
        <Toolbar disableGutters>
          <Typography variant="h6" className={classes.title}>
            Monito
          </Typography>
        </Toolbar>
        <List component="nav">
          {role === 'admin' && <ListNavLink to="settings" text="Settings" />}
          {role === 'admin' && <ListNavLink to="users" text="Users" />}
          {role === 'admin' && <ListNavLink to="bases" text="Bases" />}
          <ListNavLink to="" text="VPS" />
          <ListNavLink to="logs" text="Logs" />
          <ListNavLink to="playlists" text="Playlists" />
          <ListNavLink to="upload" text="Uploads" />
        </List>
      </Drawer>
      <main className={classes.content}>
        <AppBar color="default" variant="outlined">
          <Toolbar>
            <div style={{ flexGrow: 1 }} />
            <Button className={classes.menuButton} onClick={handleLogout}>
              Logout
            </Button>
          </Toolbar>
        </AppBar>
        <Toolbar />
        {children}
      </main>
    </div>
  )
}
