import React, { useState, useEffect } from 'react'
import { useSnackbar } from 'notistack'
import axios from 'axios'
import { makeStyles, Theme } from '@material-ui/core/styles'
import {
  Toolbar,
  Button,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Divider,
  Popover,
  TextField,
  Paper,
} from '@material-ui/core'

import FileIcon from '@material-ui/icons/InsertDriveFile'
import DeleteIcon from '@material-ui/icons/Delete'
import PublishIcon from '@material-ui/icons/Publish'
import DownloadIcon from '@material-ui/icons/GetApp'

import { Head } from '../components/Head'
import { ContentBox } from '../components/ContentBox'
import { basepath } from '../../../config'

const useStyles = makeStyles((theme: Theme) => ({
  menuButton: {
    marginLeft: theme.spacing(2),
  },
  popover: {
    padding: theme.spacing(2),
  },
}))

const Uplaod: React.FC = () => {
  const [fileNames, setFileNames] = useState([])
  const [currentFile, setCurrentFile] = useState(undefined)
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null)
  const [path, setPath] = useState('/')

  const classes = useStyles()
  const { enqueueSnackbar } = useSnackbar()

  const fetchFileNames = () => {
    axios.get('api/files').then((res) => {
      setFileNames(res.data)
    })
  }

  useEffect(() => {
    fetchFileNames()
  }, [])

  const handleUpload = (e) => {
    const file = e.target.files[0]

    const formData = new FormData()

    formData.append('file', file)

    axios
      .post('/api/files/upload', formData, {
        headers: {
          'Content-Type': 'multipart/form-data; boundary=something',
        },
      })
      .then(() => {
        enqueueSnackbar(`Successfully upload file to cloud storage`, {
          variant: 'success',
        })
        fetchFileNames()
      })
      .catch(() => {
        enqueueSnackbar(`Can't upload file to cloud storage`, {
          variant: 'error',
        })
      })
  }

  const handleDelete = (name) => {
    axios
      .get(`/api/files/remove/${name}`)
      .then(() => {
        enqueueSnackbar(`Successfully delete file from cloud storage`, {
          variant: 'success',
        })
        fetchFileNames()
      })
      .catch(() => {
        enqueueSnackbar(`Can't delete file from cloud storage`, {
          variant: 'error',
        })
      })
  }

  const handleSubmit = (event: React.SubmitEvent<HTMLButtonElement>) => {
    event.preventDefault()

    console.log(currentFile)
    axios
      .post(`/api/vps/send`, {
        name: currentFile,
        path,
      })
      .then(() => {
        enqueueSnackbar(`Successfully send file to vps`, {
          variant: 'success',
        })
      })
      .catch(() => {
        enqueueSnackbar(`Can't send file to vps`, {
          variant: 'error',
        })
      })
  }

  const handlePopClick = (
    event: React.MouseEvent<HTMLButtonElement>,
    fileName: string
  ) => {
    setCurrentFile(fileName)
    setAnchorEl(event.currentTarget)
  }

  const handlePopClose = () => {
    setAnchorEl(null)
  }

  const open = Boolean(anchorEl)
  const id = open ? 'simple-popover' : undefined

  return (
    <>
      <Head title="Uploading fiels" description="Uploading fiels" />
      <ContentBox>
        <Toolbar>
          <Button
            color="primary"
            aria-label="download all"
            href={`${basepath === '/' ? '' : basepath}/api/files/zip`}
            startIcon={<DownloadIcon />}
          >
            Download zip backup
          </Button>
          <div style={{ flexGrow: 1 }} />
          <input
            id="upload"
            style={{ display: 'none' }}
            type="file"
            name="file"
            onChange={handleUpload}
          />
          <label htmlFor="upload">
            <Button
              className={classes.menuButton}
              variant="contained"
              color="primary"
              component="span"
            >
              New File
            </Button>
          </label>
        </Toolbar>
        <Divider />
        <List>
          {fileNames.map((fileName, i) => (
            <ListItem key={i}>
              <ListItemAvatar>
                <Avatar>
                  <FileIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={fileName} />
              <ListItemSecondaryAction>
                <IconButton
                  aria-label="download"
                  href={`${
                    basepath === '/' ? '' : basepath
                  }/api/files/${fileName}`}
                >
                  <DownloadIcon />
                </IconButton>
                <IconButton
                  aria-label="publich"
                  onClick={(event) => handlePopClick(event, fileName)}
                >
                  <PublishIcon />
                </IconButton>
                <IconButton
                  aria-label="delete"
                  onClick={() => handleDelete(fileName)}
                >
                  <DeleteIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handlePopClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <Paper className={classes.popover}>
            <form onSubmit={handleSubmit}>
              <TextField
                label="relative path"
                type="text"
                InputLabelProps={{
                  shrink: true,
                }}
                value={path}
                onChange={(event) => setPath(event.target.value)}
              />
              <Button type="submit" variant="contained" color="primary">
                Send
              </Button>
            </form>
          </Paper>
        </Popover>
      </ContentBox>
    </>
  )
}

export default Uplaod
