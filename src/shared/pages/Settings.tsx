import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useFormik } from 'formik'
import { useSnackbar } from 'notistack'
import { makeStyles } from '@material-ui/core/styles'

import { Head } from '../components/Head'
import { ContentBox } from '../components/ContentBox'

import { Button, TextField, Typography, Toolbar } from '@material-ui/core'

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    padding: theme.spacing(1),
  },
  container: {
    width: '32ch',
    padding: theme.spacing(1),
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  form__field: {
    marginBottom: theme.spacing(1),
    width: '100%',
  },
  form__submit: {
    marginTop: theme.spacing(2),
  },
  form__title: {
    padding: theme.spacing(2, 0),
  },
}))

const Settings: React.FC = () => {
  const [id, setId] = useState('')

  const classes = useStyles()
  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    axios
      .get('api/settings')
      .then((res) => {
        const { _id, ...rest } = res.data
        setId(_id)
        formik.setValues({ ...rest }, false)
      })
      .catch((e) => {
        console.log(e)
      })
  }, [])

  const initialValues = {
    webhook: {
      key: '',
    },
    airtable: {
      key: '',
      bases: {
        kibo: '',
        label: '',
        stats: '',
        logs: '',
      },
    },
    fileLimit: '',
  }

  const formik = useFormik({
    initialValues,
    onSubmit: async (values) => {
      try {
        await axios.post(
          'api/settings',
          { _id: id, ...values },
          { headers: { 'Content-type': 'application/json' } }
        )
        enqueueSnackbar(`Successfully update settings`, {
          variant: 'success',
        })
      } catch (e) {
        enqueueSnackbar('Can`t update user data', {
          variant: 'error',
        })
      }
    },
  })

  return (
    <>
      <Head title="Settings" description="Platform settings" />
      <ContentBox>
        <Toolbar disableGutters>
          <Typography className={classes.title} variant="h5">
            Settings
          </Typography>
        </Toolbar>
        <div className={classes.container}>
          <form
            className={classes.form}
            noValidate
            autoComplete="off"
            onSubmit={formik.handleSubmit}
          >
            <Typography className={classes.form__title} variant="h6">
              Api keys
            </Typography>
            <TextField
              className={classes.form__field}
              id="webhook.key"
              name="webhook.key"
              label="Webhook"
              value={formik.values.webhook.key}
              onChange={formik.handleChange}
            />
            <TextField
              className={classes.form__field}
              id="airtable.key"
              name="airtable.key"
              label="Airtable"
              value={formik.values.airtable.key}
              onChange={formik.handleChange}
            />
            <Typography className={classes.form__title} variant="h6">
              Bases
            </Typography>
            <TextField
              className={classes.form__field}
              id="airtable.bases.kibo"
              name="airtable.bases.kibo"
              label="Kibo"
              value={formik.values.airtable.bases.kibo}
              onChange={formik.handleChange}
            />
            <TextField
              className={classes.form__field}
              id="airtable.bases.label"
              name="airtable.bases.label"
              label="Label"
              value={formik.values.airtable.bases.label}
              onChange={formik.handleChange}
            />
            <TextField
              className={classes.form__field}
              id="airtable.bases.stats"
              name="airtable.bases.stats"
              label="Stats"
              value={formik.values.airtable.bases.stats}
              onChange={formik.handleChange}
            />
            <TextField
              className={classes.form__field}
              id="airtable.bases.logs"
              name="airtable.bases.logs"
              label="Logs"
              value={formik.values.airtable.bases.logs}
              onChange={formik.handleChange}
            />
            <Typography className={classes.form__title} variant="h6">
              File limit
            </Typography>
            <TextField
              className={classes.form__field}
              id="fileLimit"
              name="fileLimit"
              label="File limit"
              value={formik.values.fileLimit}
              onChange={formik.handleChange}
            />
            <Button
              className={classes.form__submit}
              type="submit"
              variant="contained"
              color="primary"
            >
              Update
            </Button>
          </form>
        </div>
      </ContentBox>
    </>
  )
}

export default Settings
