import React from 'react'
import { Redirect } from '@reach/router'
import { AuthContext } from './AuthProvider'

export interface CustomRouteProps {
  component: React.ReactType;
  access: string;
}

export const ProtectedRoute: React.FC = ({
  component: Component,
  access = 'moderator',
  ...rest
}: CustomRouteProps) => {
  const { role } = React.useContext(AuthContext)

  const level = {
    admin: 2,
    moderator: 1,
    unauthorized: 0,
  }
  return level[role] >= level[access] ? (
    <Component {...rest} />
  ) : (
    <Redirect to="login" noThrow />
  )
}

export const PublicRoute: React.FC = ({
  component: Component,
  ...rest
}: CustomRouteProps) => <Component {...rest} />
