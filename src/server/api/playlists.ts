import express from 'express'
import axios from 'axios'

import { accessMiddleware } from '../accessMiddleware'
import { webhook } from '../../../config'
import { getApiKey } from '../getApiKey'

const playlistRouter = express.Router()

playlistRouter.get('/', accessMiddleware('moderator'), async (req, res) => {
  const responce = await axios({
    method: 'GET',
    url: `${webhook}/getTracksSelections`,
    headers: { 'x-api-key': await getApiKey() },
  })
  res.send(responce.data)
})

playlistRouter.get(
  '/unsubscribe',
  accessMiddleware('moderator'),
  async (req, res) => {
    const responce = await axios({
      method: 'GET',
      url: `${webhook}/getPlsForDelete`,
      headers: { 'x-api-key': await getApiKey() },
    })
    res.send(responce.data)
  }
)

playlistRouter.post('/send', async (req, res) => {
  try {
    await axios({
      method: 'POST',
      url: `${webhook}/sendPlaylistsURIs`,
      headers: { 'x-api-key': await getApiKey() },
      data: req.body,
    })
    res.end()
  } catch (e) {
    res.status(403).end()
  }
})

playlistRouter.get(
  '/reserve',
  accessMiddleware('moderator'),
  async (req, res) => {
    const responce = await axios({
      method: 'GET',
      url: `${webhook}/getTracksSelections/reserve`,
      headers: { 'x-api-key': await getApiKey() },
    })
    res.send(responce.data)
  }
)

playlistRouter.get(
  '/unsubscribe/reserve',
  accessMiddleware('moderator'),
  async (req, res) => {
    const responce = await axios({
      method: 'GET',
      url: `${webhook}/getPlsForDelete/reserve`,
      headers: { 'x-api-key': await getApiKey() },
    })
    res.send(responce.data)
  }
)

playlistRouter.post('/send/reserve', async (req, res) => {
  try {
    await axios({
      method: 'POST',
      url: `${webhook}/sendPlaylistsURIs/reserve`,
      headers: { 'x-api-key': await getApiKey() },
      data: req.body,
    })
    res.end()
  } catch (e) {
    res.status(403).end()
  }
})

export default playlistRouter
