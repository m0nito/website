import express from 'express'
import bcrypt from 'bcrypt'

import { accessMiddleware } from '../accessMiddleware'
import { Users } from '../db'

const userRouter = express.Router()

userRouter.get('/', accessMiddleware('admin'), async (req, res) => {
  const users = await Users.find({}, '-encryptedPassword')
  res.send(users)
})

userRouter.get('/:id', accessMiddleware('admin'), async (req, res) => {
  let user
  try {
    user = await Users.findById(req.params.id, '-encryptedPassword')
    res.send(user)
  } catch (e) {
    res.status(404).end()
  }
})

userRouter.post('/create', accessMiddleware('admin'), async (req, res) => {
  const { email, password, repeat, role } = req.body

  if (!!password && !!repeat && password === repeat) {
    const encryptedPassword = await bcrypt.hash(password, 10)
    const user = new Users({ email, encryptedPassword, role })

    user.save((err, doc) => {
      if (err) {
        res.status(500).end()
      } else {
        res.send(doc)
      }
    })
  } else {
    res.status(500).end()
  }
})

userRouter.post('/update', accessMiddleware('admin'), async (req, res) => {
  const { _id, fields } = req.body
  const { email, password, repeat, role } = fields

  if (password !== repeat) {
    res.status(500).end()
    return
  }

  const isPwdUpdated = !!password && !!repeat

  const newData = {
    email,
    role,
    modified: Date.now(),
    ...(isPwdUpdated && {
      encryptedPassword: await bcrypt.hash(password, 10),
    }),
  }

  Users.findByIdAndUpdate(_id, newData, (err, doc) => {
    if (err) {
      res.status(404).end()
    } else {
      req.session.role = doc.role
      res.send(doc)
    }
  })
})

userRouter.post('/delete', accessMiddleware('admin'), (req, res) => {
  const { _id } = req.body
  Users.findByIdAndDelete(_id, (err, doc) => {
    if (err) {
      res.status(404).end()
    } else {
      res.send(doc)
    }
  })
})

export default userRouter
