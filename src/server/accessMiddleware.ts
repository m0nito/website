import { Request, Response, NextFunction } from 'express'

const level = {
  admin: 2,
  moderator: 1,
  unauthorized: 0,
}

export const accessMiddleware = (access: string) => (
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  if (req.session.role && level[req.session.role] >= level[access]) {
    next()
  } else {
    res.status(403).end()
  }
}
