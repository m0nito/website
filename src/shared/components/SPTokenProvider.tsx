import React from 'react'

// eslint-disable-next-line
const noop = () => {}

export interface SPTokenProviderProps {
  tokenProp?: string | boolean;
}

export const SPTokenContext = React.createContext({
  token: false,
  setToken: noop,
})

export const SPTokenProvider: React.FC = ({
  tokenProp = false,
  children,
}: SPTokenProviderProps) => {
  const [token, setToken] = React.useState(tokenProp)

  const context = React.useMemo(() => {
    return {
      token,
      setToken,
    }
  }, [token])

  return (
    <SPTokenContext.Provider value={context}>
      {children}
    </SPTokenContext.Provider>
  )
}
