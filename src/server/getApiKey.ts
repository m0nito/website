import { Settings } from './db'

export const getApiKey = async (): string => {
  return (await Settings.find({}).lean())[0].webhook.key
}
