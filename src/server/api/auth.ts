import express from 'express'
import bcrypt from 'bcrypt'

import { Users } from '../db'

const authRouter = express.Router()

authRouter.post('/login', async (req, res) => {
  const { email, password } = req.body
  const user = await Users.findOne({ email })
  if (user) {
    const passwordMatch = await bcrypt.compare(password, user.encryptedPassword)
    if (passwordMatch) {
      Users.updateOne({ email }, { lastLogin: Date.now() }).exec()
      req.session.role = user.role
      res.send(user)
    } else {
      res.status(403).end()
    }
  } else {
    res.status(403).end()
  }
})

authRouter.get('/logout', (req, res) => {
  req.session.destroy()
  res.end()
})

export default authRouter
