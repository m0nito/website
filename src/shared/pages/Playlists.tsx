import React, { useEffect, useContext } from 'react'
import { navigate, useLocation } from '@reach/router'
import { Button, Divider, Toolbar } from '@material-ui/core'
import { useSnackbar } from 'notistack'

import { Head } from '../components/Head'
import { ContentBox } from '../components/ContentBox'
import { SPTokenContext } from '../components/SPTokenProvider'

import syncPlaylists from '../requests/syncPlaylists'
import unsubscribeFromPlaylsits from '../requests/unsubscribeFromPlaylists'

const Playlists: React.FC = () => {
  const { token, setToken } = useContext(SPTokenContext)
  const location = useLocation()
  const { enqueueSnackbar } = useSnackbar()

  const clientId = 'e107cf543ffa4cd2982afd5152a3ebdf'
  const redirectUri = 'https://monito.website/asn/playlists'
  //const redirectUri = 'http://localhost:8000/playlists'

  const scopes = [
    'playlist-modify-public',
    'playlist-modify-private',
    'playlist-read-collaborative',
  ]

  const handleGetToken = () => {
    navigate(
      `https://accounts.spotify.com/authorize?client_id=${clientId}&redirect_uri=${redirectUri}&scope=${scopes.join(
        '%20'
      )}&response_type=token&show_dialog=true`
    )
  }

  const handleDelToken = () => {
    setToken(false)
    navigate('playlists')
  }

  const handlePlayLists = ({ isAlternateEndpoint }) => {
    syncPlaylists({
      token: token,
      onFinish: (successCounter, counter) => {
        enqueueSnackbar(
          `Create or Update ${successCounter}/${counter} sp playlists`,
          { variant: 'info' }
        )
      },
      onActionFailure: (playlistId) => {
        enqueueSnackbar(`Playlist ${playlistId} does't exist on user acc`, {
          variant: 'error',
        })
      },
      onFailure: () => {
        enqueueSnackbar(`Can't create sp playlists`, {
          variant: 'error',
        })
      },
      isAlternateEndpoint,
    })
  }

  const handleUnsubscribeFromPlayLists = ({ isAlternateEndpoint }) => {
    unsubscribeFromPlaylsits({
      token: token,
      onFinish: (successCounter, counter) => {
        enqueueSnackbar(
          `unsubscribed from ${successCounter}/${counter} sp playlists`,
          {
            variant: 'info',
          }
        )
      },
      onActionFailure: (errMessage) => {
        enqueueSnackbar(errMessage, {
          variant: 'error',
        })
      },
      onFailure: () => {
        enqueueSnackbar(`Can't unsubscribed from sp playlists`, {
          variant: 'error',
        })
      },
      isAlternateEndpoint,
    })
  }

  useEffect(() => {
    if (!token) {
      const hash = location.hash
        .substring(1)
        .split('&')
        .reduce(function (initial, item) {
          if (item) {
            const parts = item.split('=')
            initial[parts[0]] = decodeURIComponent(parts[1])
          }
          return initial
        }, {})

      const accessToken = hash.access_token

      if (accessToken) {
        setToken(accessToken)
        navigate('playlists')
        enqueueSnackbar(`Successfully get Spotify token`, {
          variant: 'success',
        })
      }
    }
  }, [])

  return (
    <>
      <Head title="VPS Control panel" description="VPS Control panel" />
      <ContentBox>
        <Toolbar disableGutters>
          {token ? (
            <Button
              color="secondary"
              variant="contained"
              onClick={handleDelToken}
            >
              Delete spotify token
            </Button>
          ) : (
            <Button
              color="secondary"
              variant="contained"
              onClick={handleGetToken}
            >
              Get spotify token
            </Button>
          )}
        </Toolbar>
        {token && (
          <div>
            <p>
              <b>Token</b>: {token}
            </p>
            <Divider />
            <h2>Kibo</h2>
            <p>
              <Button
                color="primary"
                variant="contained"
                onClick={() => handlePlayLists({ isAlternateEndpoint: false })}
              >
                Create SP playlists
              </Button>
            </p>
            <p>
              <Button
                color="default"
                variant="outlined"
                onClick={() =>
                  handleUnsubscribeFromPlayLists({ isAlternateEndpoint: false })
                }
              >
                unsubscribe from SP playlists
              </Button>
            </p>
            <h2>Label</h2>
            <p>
              <Button
                color="primary"
                variant="contained"
                onClick={() => handlePlayLists({ isAlternateEndpoint: true })}
              >
                Create SP playlists
              </Button>
            </p>
            <p>
              <Button
                color="default"
                variant="outlined"
                onClick={() =>
                  handleUnsubscribeFromPlayLists({ isAlternateEndpoint: true })
                }
              >
                unsubscribe from SP playlists
              </Button>
            </p>
          </div>
        )}
      </ContentBox>
    </>
  )
}

export default Playlists
