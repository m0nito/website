import React from 'react'

import { Head } from '../components/Head'
import { VpsTable } from '../components/VpsTable'

const Vps: React.FC = () => (
  <>
    <Head title="VPS Control panel" description="VPS Control panel" />
    <VpsTable />
  </>
)

export default Vps
