import fs from 'fs'
import express from 'express'
import multer from 'multer'
import archiver from 'archiver'
import { uploadFolder } from '../../../config'

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, uploadFolder + '/')
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname)
  },
})

const upload = multer({ storage: storage })

const fileRouter = express.Router()

fileRouter.get('/', (req, res) => {
  fs.readdir(uploadFolder, (err, files) => {
    res.send(files)
  })
})

fileRouter.get('/zip', (req, res) => {
  res.attachment('backup.zip')

  const archive = archiver('zip')

  archive.on('error', (e) => {
    console.log(e)
  })

  archive.pipe(res)

  archive.directory(uploadFolder, false)
  archive.finalize()
})

fileRouter.get('/:filename', (req, res) => {
  const { filename } = req.params
  res.download(`${uploadFolder}/${filename}`)
})

fileRouter.get('/remove/:filename', (req, res) => {
  const { filename } = req.params
  fs.unlink(`${uploadFolder}/${filename}`, (err) => {
    if (err) res.status(404).send(err)
    res.end()
  })
})

fileRouter.post('/upload', upload.single('file'), (req, res) => {
  res.end()
})

export default fileRouter
