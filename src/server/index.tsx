import path from 'path'
import express from 'express'
import session from 'express-session'
import MongoStore from 'connect-mongo'
import mongoose from 'mongoose'
import React from 'react'
import { ChunkExtractor } from '@loadable/server'
import { renderToString } from 'react-dom/server'
import { ServerLocation } from '@reach/router'
import { Helmet } from 'react-helmet'
import { ServerStyleSheets, ThemeProvider } from '@material-ui/core/styles'
import { SnackbarProvider } from 'notistack'

import htmlTemplate from './htmlTemplate'
import App from '../shared/App'
import theme from '../shared/theme'
import api from './api'

import { port, basepath, dbHost, dbConfig } from '../../config'
import { AuthProvider } from '../shared/components/AuthProvider'
import { SPTokenProvider } from '../shared/components/SPTokenProvider'

const app = express()

// TODO secure cookies
app.use(
  session({
    store: MongoStore.create({ mongoUrl: dbHost }),
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
  })
)

app.use(express.static('dist'))

app.use('/api', api)

const statsFile = path.resolve('./dist/loadable-stats.json')

app.get('*', (req, res) => {
  const sheets = new ServerStyleSheets()
  const extractor = new ChunkExtractor({ statsFile })

  const role = req.session.role || 'unauthorized'

  const markup = renderToString(
    extractor.collectChunks(
      sheets.collect(
        <ThemeProvider theme={theme}>
          <SnackbarProvider maxSnack={3}>
            <ServerLocation url={`${basepath}${req.url}`}>
              <AuthProvider roleProp={role}>
                <SPTokenProvider>
                  <App />
                </SPTokenProvider>
              </AuthProvider>
            </ServerLocation>
          </SnackbarProvider>
        </ThemeProvider>
      )
    )
  )

  const helmet = Helmet.renderStatic()
  const scripts = extractor.getScriptTags()
  const css = sheets.toString()

  res.send(htmlTemplate({ html: markup, helmet, scripts, css, role }))
})

const run = async () => {
  await mongoose.connect(dbHost, dbConfig)
  await app.listen(port, () => console.log(`app started on ${port} port`))
}

run()
