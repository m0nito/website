import { v4 as uuidv4 } from 'uuid'
import mongoose from 'mongoose'

export const Users = mongoose.model('Users', {
  email: { type: String, required: true },
  encryptedPassword: { type: String, required: true },
  role: { type: String, enum: ['admin', 'moderator'], required: true },
  created: { type: Date, default: Date.now },
  modified: { type: Date, default: Date.now },
  lastLogin: { type: Date },
})

export const Vps = mongoose.model('Vps', {
  id: { type: String, required: true },
  apiKey: { type: String, default: () => uuidv4() },
  created: { type: Date, default: Date.now },
  modified: { type: Date, default: Date.now },
  calls: { type: Number, default: 0 },
})

export const Logs = mongoose.model('Logs', {
  app: { type: String, required: true },
  vps: { type: String, required: true },
  type: { type: String, required: true },
  value: { type: String },
  timestamp: { type: Date, default: Date.now },
})

export const Settings = mongoose.model('Settings', {
  webhook: {
    key: String,
  },
  airtable: {
    key: String,
    bases: {
      kibo: String,
      label: String,
      stats: String,
      logs: String,
    },
  },
  fileLimit: Number,
})

export const Bases = mongoose.model('Bases', {
  name: { type: String, require: true },
  baseId: { type: String, require: true },
  apiKey: { type: String, require: true },
  created: { type: Date, default: Date.now },
  modified: { type: Date, default: Date.now },
})
