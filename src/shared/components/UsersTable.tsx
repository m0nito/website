import React, { useState, useEffect, forwardRef } from 'react'
import { Link, navigate } from '@reach/router'
import axios from 'axios'
import MaterialTable from 'material-table'
import Add from '@material-ui/icons/Add'
import { useSnackbar } from 'notistack'

import { tableIcons } from './tableIcons'
import { ContentBox } from './ContentBox'

export const UsersTable: React.FC = () => {
  const { enqueueSnackbar } = useSnackbar()

  const [data, setData] = useState([])

  useEffect(() => {
    axios
      .get('api/users')
      .then((res) => setData(res.data))
      .catch(() =>
        enqueueSnackbar('Can`t load data from server', {
          variant: 'error',
        })
      )
  }, [])

  return (
    <MaterialTable
      icons={tableIcons}
      columns={[
        {
          title: 'email',
          field: 'email',
          render: (rowData) => <Link to={rowData._id}>{rowData.email}</Link>,
        },
        {
          title: 'role',
          field: 'role',
        },
        {
          title: 'created',
          field: 'created',
          editable: 'never',
          type: 'datetime',
        },
        {
          title: 'modified',
          field: 'modified',
          editable: 'never',
          type: 'datetime',
        },
        {
          title: 'last login',
          field: 'lastLogin',
          editable: 'never',
          type: 'datetime',
        },
      ]}
      data={data}
      title="Users"
      actions={[
        {
          icon: forwardRef((props, ref) => <Add {...props} ref={ref} />),
          tooltip: 'Add User',
          isFreeAction: true,
          onClick: () => navigate('users/new'),
        },
      ]}
      editable={{
        onRowDelete: (oldData) =>
          new Promise((resolve, reject) => {
            const { _id } = oldData

            axios
              .post('api/users/delete', { _id })
              .then((res) => {
                const dataDelete = [...data]
                const index = oldData.tableData.id
                dataDelete.splice(index, 1)
                setData([...dataDelete])
                enqueueSnackbar(
                  `Successfully delete user (email: ${res.data.email})`,
                  {
                    variant: 'success',
                  }
                )
                resolve()
              })
              .catch((e) => {
                enqueueSnackbar('Can`t delete user', {
                  variant: 'error',
                })
                reject(e)
              })
          }),
      }}
      components={{
        Container: (props) => <ContentBox {...props} />,
      }}
    />
  )
}
