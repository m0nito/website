import axios from 'axios'
import React from 'react'
import { Router } from '@reach/router'
import loadable from '@loadable/component'
import CssBaseline from '@material-ui/core/CssBaseline'

import { PublicRoute, ProtectedRoute } from './components/routes'

import { basepath, port } from '../../config'

axios.defaults.baseURL =
  basepath !== '/'
    ? `https://monito.website${basepath}`
    : `http://localhost:${port}`

const Layout = loadable(() => import('./components/Layout'))
const Vps = loadable(() => import('./pages/Vps'))
const Users = loadable(() => import('./pages/Users'))
const User = loadable(() => import('./pages/User'))
const NewUser = loadable(() => import('./pages/NewUser'))
const NotFound = loadable(() => import('./pages/NotFound'))
const Login = loadable(() => import('./pages/Login'))
const Logs = loadable(() => import('./pages/Logs'))
const Playlists = loadable(() => import('./pages/Playlists'))
const Settings = loadable(() => import('./pages/Settings'))
const Upload = loadable(() => import('./pages/Upload'))
const Bases = loadable(() => import('./pages/Bases'))

const App: React.FC = () => (
  <>
    <CssBaseline />
    <Router basepath={basepath}>
      <ProtectedRoute path="/" component={Layout}>
        <ProtectedRoute path="/" component={Vps} />
        <ProtectedRoute path="logs" component={Logs} />
        <ProtectedRoute path="upload" component={Upload} />
        <ProtectedRoute path="users" component={Users} access="admin" />
        <ProtectedRoute path="users/:id" component={User} access="admin" />
        <ProtectedRoute path="users/new" component={NewUser} access="admin" />
        <ProtectedRoute path="settings" component={Settings} access="admin" />
        <ProtectedRoute path="bases" component={Bases} access="admin" />
        <ProtectedRoute path="playlists" component={Playlists} />
      </ProtectedRoute>
      <PublicRoute path="login" component={Login} />
      <PublicRoute default component={NotFound} />
    </Router>
  </>
)

export default App
