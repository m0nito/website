import fs from 'fs'
import express from 'express'
import axios from 'axios'
import archiver from 'archiver'

import { uploadFolder } from '../../../config'
import { Settings } from '../db'

/* helpers */

function getCurrentDateString(): string {
  return new Date(Date.now()).toISOString()
}

function getVersionedFiles(name: string, format: string): string[] {
  return fs
    .readdirSync(uploadFolder)
    .filter(
      (filename) =>
        filename.split('.').slice(-1)[0] === format &&
        filename.split('_')[0] === name
    )
    .sort((a, b) => {
      const aDateString = a.split('_')[1]
      const bDateString = b.split('_')[1]
      return aDateString > bDateString ? -1 : aDateString < bDateString ? 1 : 0
    })
}

async function getFileLimit(): number {
  const fileLimit = (await Settings.find({}).lean())[0]?.fileLimit
  return fileLimit > 0 ? fileLimit : 9999999
}

/* router instance */

const webhookRouter = express.Router()

/* webhook middleware */

webhookRouter.use(async (req, res, next) => {
  try {
    const settings = (await Settings.find({}).lean())[0]

    req.headers['x-api-key'] === settings.webhook.key
      ? next()
      : res.status(403).end()
  } catch (e) {
    res.status(500).end(e)
  }
})

/* routes */

webhookRouter.post('/backup', async (req, res) => {
  const { url, filename } = req.body

  const fileNameArr = filename.split('.')

  const format = fileNameArr[fileNameArr.length - 1]

  const name = fileNameArr.slice(0, fileNameArr.length - 1).join('.')

  const versions = getVersionedFiles(name, format)

  if (versions.length >= (await getFileLimit())) {
    fs.unlinkSync(`${uploadFolder}/${versions.pop()}`)
  }

  const response = await axios({
    url,
    method: 'GET',
    responseType: 'stream',
  })

  const writer = fs.createWriteStream(
    `${uploadFolder}/${name}_${getCurrentDateString()}.${format}`
  )

  writer.on('error', (e) => {
    console.log(e)
  })

  response.data.pipe(writer)
  res.end()
})

// TODO: Server side events
webhookRouter.post('/zip', async (req, res) => {
  const { files, zipName } = req.body

  res.end()

  const versions = getVersionedFiles(zipName, 'zip')

  console.log(versions)

  if (versions.length >= (await getFileLimit())) {
    fs.unlinkSync(`${uploadFolder}/${versions.pop()}`)
  }

  const output = fs.createWriteStream(
    `${uploadFolder}/${zipName}_${getCurrentDateString()}.zip`
  )

  const archive = archiver('zip')

  archive.on('error', (e) => {
    console.log(e)
  })

  archive.pipe(output)

  for (const { url, name } of files) {
    try {
      const response = await axios({
        url,
        method: 'GET',
        responseType: 'stream',
      })

      archive.append(response.data, { name })
    } catch (e) {
      console.log(e)
    }
  }

  archive.finalize()
})

webhookRouter.post('/createFile', async (req, res) => {
  const { filename, data } = req.body

  const [name, format] = filename.split('.')

  const versions = getVersionedFiles(name, format)

  if (versions.length >= (await getFileLimit())) {
    fs.unlinkSync(`${uploadFolder}/${versions.pop()}`)
  }

  fs.writeFileSync(
    `${uploadFolder}/${name}_${getCurrentDateString()}.${format}`,
    data
  )
  res.end()
})

export default webhookRouter
