import React, { useState, useEffect } from 'react'
import axios from 'axios'
import MaterialTable from 'material-table'
import { useSnackbar } from 'notistack'

import { tableIcons } from './tableIcons'
import { ContentBox } from './ContentBox'

export const VpsTable: React.FC = () => {
  const { enqueueSnackbar } = useSnackbar()

  const [data, setData] = useState([])

  useEffect(() => {
    axios
      .get('api/vps')
      .then((res) => setData(res.data))
      .catch(() =>
        enqueueSnackbar('Can`t load data from server', {
          variant: 'error',
        })
      )
  }, [])

  return (
    <MaterialTable
      icons={tableIcons}
      columns={[
        { title: 'id', field: 'id' },
        { title: 'apiKey', field: 'apiKey', editable: 'never' },
        {
          title: 'created',
          field: 'created',
          editable: 'never',
          type: 'datetime',
        },
        {
          title: 'modified',
          field: 'modified',
          editable: 'never',
          type: 'datetime',
        },
        {
          title: 'calls',
          field: 'calls',
          editable: 'never',
          type: 'numeric',
        },
      ]}
      data={data}
      title="Servers"
      editable={{
        onRowAdd: (newData) =>
          new Promise((resolve, reject) => {
            const { id } = newData
            axios
              .post('api/vps/create', { id })
              .then((res) => {
                setData([...data, res.data])
                enqueueSnackbar(
                  `Successfully add server data (id: ${res.data.id})`,
                  {
                    variant: 'success',
                  }
                )
                resolve()
              })
              .catch((e) => {
                enqueueSnackbar('Can`t create server data', {
                  variant: 'error',
                })
                reject(e)
              })
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(async (resolve, reject) => {
            const { _id, id, modified } = newData
            axios
              .post('api/vps/update', { _id, fields: { id, modified } })
              .then((res) => {
                const dataUpdate = [...data]
                const index = oldData.tableData.id
                dataUpdate[index] = { ...newData, modified: res.data.modified }
                setData([...dataUpdate])
                enqueueSnackbar('Successfully update server data', {
                  variant: 'success',
                })
                resolve()
              })
              .catch((e) => {
                enqueueSnackbar('Can`t update server data', {
                  variant: 'error',
                })
                reject(e)
              })
          }),
        onRowDelete: (oldData) =>
          new Promise((resolve, reject) => {
            const { _id } = oldData

            axios
              .post('api/vps/delete', { _id })
              .then((res) => {
                const dataDelete = [...data]
                const index = oldData.tableData.id
                dataDelete.splice(index, 1)
                setData([...dataDelete])
                enqueueSnackbar(
                  `Successfully delete server data (id: ${res.data.id})`,
                  {
                    variant: 'success',
                  }
                )
                resolve()
              })
              .catch((e) => {
                enqueueSnackbar('Can`t delete server data', {
                  variant: 'error',
                })
                reject(e)
              })
          }),
      }}
      components={{
        Container: (props) => <ContentBox {...props} />,
      }}
    />
  )
}
