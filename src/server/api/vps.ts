import express from 'express'
import axios from 'axios'

import { Vps } from '../db'
import { webhook } from '../../../config'
import { getApiKey } from '../getApiKey'

const vpsRouter = express.Router()

vpsRouter.get('/', async (req, res) => {
  const vps = await Vps.find()
  res.send(vps)
})

vpsRouter.post('/update', (req, res) => {
  const { _id, fields } = req.body
  Vps.findByIdAndUpdate(
    _id,
    { ...fields, modified: Date.now() },
    (err, doc) => {
      if (err) {
        res.status(404).end()
      } else {
        res.send(doc)
      }
    }
  )
})

vpsRouter.post('/delete', (req, res) => {
  const { _id } = req.body
  Vps.findByIdAndDelete(_id, (err, doc) => {
    if (err) {
      res.status(404).end()
    } else {
      res.send(doc)
    }
  })
})

vpsRouter.post('/create', (req, res) => {
  const { id } = req.body
  const vps = new Vps({ id })

  vps.save((err, doc) => {
    if (err) {
      res.status(500).end()
    } else {
      res.send(doc)
    }
  })
})

vpsRouter.post('/send', async (req, res) => {
  try {
    await axios({
      method: 'POST',
      url: `${webhook}/sendLocalFiles`,
      headers: { 'x-api-key': await getApiKey() },
      data: req.body,
    })
    res.end()
  } catch (e) {
    res.status(403).end()
  }
})

export default vpsRouter
