import React from 'react'

import { Head } from '../components/Head'
import { UsersTable } from '../components/UsersTable'

const Users: React.FC = () => (
  <>
    <Head title="Users Control panel" description="Users Control panel" />
    <UsersTable />
  </>
)

export default Users
