module.exports = {
  basepath: '/inv',
  port: 9000,
  dbHost: 'mongodb://localhost/inv',
  webhook: 'https://monito.website/inv/controller/v2/webhook',
  dbConfig: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  },
  uploadFolder: 'uploads',
}
