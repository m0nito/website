module.exports = {
  basepath: '/asn',
  port: 8000,
  dbHost: 'mongodb://localhost/test',
  webhook: 'https://monito.website/asn/controller/v2/webhook',
  dbConfig: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  },
  uploadFolder: 'uploads',
}
