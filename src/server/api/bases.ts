import express from 'express'
import { Bases } from '../db'

const baseRouter = express.Router()

baseRouter.get('/', async (req, res) => {
  const bases = await Bases.find()
  res.send(bases)
})

baseRouter.post('/update', (req, res) => {
  const { _id, fields } = req.body
  Bases.findByIdAndUpdate(
    _id,
    { ...fields, modified: Date.now() },
    (err, doc) => {
      if (err) {
        res.status(404).end()
      } else {
        res.send(doc)
      }
    }
  )
})

baseRouter.post('/delete', (req, res) => {
  const { _id } = req.body
  Bases.findByIdAndDelete(_id, (err, doc) => {
    if (err) {
      res.status(404).end()
    } else {
      res.send(doc)
    }
  })
})

baseRouter.post('/create', (req, res) => {
  const base = new Bases(req.body)

  base.save((err, doc) => {
    if (err) {
      res.status(500).end()
    } else {
      res.send(doc)
    }
  })
})

export default baseRouter
