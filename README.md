# monito/website

требования:

- NodeJS LTS

установка:

- запустить следующую команду в корне проекта:
    ```
        npm i
    ```

запуск в режиме разработки:
    ```
        npm run dev
    ```

сборка проекта
    ```
        npm run build
    ```

запуск:

- локальный: `npm start`
- публичный (prod): `NODE_ENV=production pm2 start server.js -n webapp`