import React, { useState } from 'react'
import moment from 'moment'
import axios from 'axios'
import {
  TextField,
  Button,
  Typography,
  Toolbar,
} from '@material-ui/core'

import { useSnackbar } from 'notistack'
import { makeStyles } from '@material-ui/core/styles'

import { Head } from '../components/Head'
import { ContentBox } from '../components/ContentBox'
//import downloadLogs from '../requests/downloadLogs'

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    padding: theme.spacing(1),
    flexGrow: 1,
  },
  container: {
    width: '32ch',
    padding: theme.spacing(1),
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  form__field: {
    marginBottom: theme.spacing(1),
    width: '100%',
  },
  form__submit: {
    marginTop: theme.spacing(2),
  },
}))

const defaultValue = {
  startDate: moment.utc().subtract(1, 'days').format("YYYY-MM-DDTHH:mm"),
  endDate: moment.utc().format("YYYY-MM-DDTHH:mm"),
  type: '',
  vps: '',
  app: '',
}

const Logs: React.FC = () => {
  const classes = useStyles()
  const { enqueueSnackbar } = useSnackbar()
  const [values, setValues] = useState<State>(defaultValue)

  // eslint-disable-next-line
  const handleChange = (prop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleSubmit = async (event: React.SubmitEvent<HTMLButtonElement>) => {

    event.preventDefault()

    try{
      const res = await axios.post('api/logs', {
        ...values,
        startDate: moment.utc(values.startDate).format("x"),
        endDate: moment.utc(values.endDate).format("x")
      }, {
        responseType: 'blob'
      })
        
      const url = window.URL.createObjectURL(new Blob([res.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'file.csv');
      link.click();
      window.URL.revokeObjectURL(url)
      
      enqueueSnackbar('Successfully download csv',{ 
        variant: 'success',
      });
    }catch(e){
      enqueueSnackbar('Can`t download csv',{ 
        variant: 'error',
      });
    }
  }

  const handleDelete = async (event: React.ClickEvent<HTMLButtonElement>) => {

    event.preventDefault()

    try{
      await axios.get('api/purge')
      enqueueSnackbar('Successfully delete logs from db',{ 
        variant: 'success',
      });
    } catch(e){
      enqueueSnackbar('Can`t delete logs from db',{ 
        variant: 'error',
      });
    }
  }

  return (
    <>
      <Head title="Logs builder" description="Logs builder" />
      <ContentBox>
        <Toolbar disableGutters>
          <Typography className={classes.title} variant="h5">
            Log builder
          </Typography>
          <Button variant="contained" color="secondary" onClick={handleDelete}>Purge Data Base</Button>
        </Toolbar>
        <div className={classes.container}>
          <form
            className={classes.form}
            onSubmit={handleSubmit}
            noValidate
            autoComplete="off"
          >
            <TextField
              label="Start date"
              type="datetime-local"
              className={classes.form__field}
              InputLabelProps={{
                shrink: true,
              }} 
              value={values.startDate}
              onChange={handleChange('startDate')}
            />
             <TextField
              label="End date"
              type="datetime-local"
              className={classes.form__field}
              InputLabelProps={{
                shrink: true,
              }} 
              value={values.endDate}
              onChange={handleChange('endDate')}
            />
            <TextField
              label="Log type"
              type="text"
              className={classes.form__field}
              InputLabelProps={{
                shrink: true,
              }}
              value={values.type}
              onChange={handleChange('type')}
            />
             <TextField
              label="VPS ID"
              type="text"
              className={classes.form__field}
              InputLabelProps={{
                shrink: true,
              }}
              value={values.vps}
              onChange={handleChange('vps')}
            />
             <TextField
              label="App ID"
              type="text"
              className={classes.form__field}
              InputLabelProps={{
                shrink: true,
              }}
              value={values.app}
              onChange={handleChange('app')}
            />
            <Button
              className={classes.form__submit}
              type="submit"
              variant="contained"
              color="primary"
            >
              Get CSV
            </Button>
          </form>
        </div>
      </ContentBox>
    </>
  )
}

export default Logs
