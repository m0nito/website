import React from 'react'
import axios from 'axios'
import { useNavigate } from '@reach/router'
import { makeStyles } from '@material-ui/core/styles'
import {
  Button,
  Typography,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  IconButton
} from '@material-ui/core'
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { useSnackbar } from 'notistack';


import { AuthContext } from '../components/AuthProvider'
import { Head } from '../components/Head'
import { basepath } from '../../../config'

const useStyles = makeStyles((theme) => ({
  root: {
    widht: '100vw',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form__field: {
    margin: theme.spacing(1),
    width: '30ch',
  },
  form__submit: {
    marginTop: theme.spacing(2),
  },
}))


interface State {
  email: string;
  password: string;
  showPassword: boolean;
}

const defaultValue = {
  email: '',
  password: '',
  showPassword: false,
}

const Login: React.FC = () => {

  const classes = useStyles()
  const navigate = useNavigate()
  const { enqueueSnackbar } = useSnackbar();

  const { setRole } = React.useContext(AuthContext)

  const [values, setValues] = React.useState<State>(defaultValue);

  // eslint-disable-next-line
  const handleChange = (prop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
  };

  const handleSubmit = async (event: React.SubmitEvent<HTMLButtonElement>) => {

    event.preventDefault()

    try{
      const res = await axios.post('api/login', { email: values.email, password: values.password })
      setRole(res.data.role)
      enqueueSnackbar(`Successfully login ${res.data.role}`,{ 
        variant: 'success',
      });
      navigate(basepath)
    }catch(e){
      enqueueSnackbar('Incorrect login or password',{ 
        variant: 'error',
      });
    }
  }

  return (
    <div>
      <Head title="Login" description="Login page" />
      <div className={classes.root}>
        <Card className={classes.container}>
          <CardContent>
            <Typography align="center" variant="h4">
              Monito
            </Typography>
            <form
              onSubmit={handleSubmit}
              className={classes.form}
            >
              <TextField
                type="email"
                value={values.email}
                onChange={handleChange('email')}
                className={classes.form__field}
                id="email"
                name="email"
                label="Email"
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                type={values.showPassword ? 'text' : 'password'}
                value={values.password}
                onChange={handleChange('password')}
                className={classes.form__field}
                id="password"
                name="password"
                label="Password" 
                InputLabelProps={{
                  shrink: true,
                }}
                InputProps={{  
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                    >
                      {values.showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
              />
              <Button
                className={classes.form__submit}
                type="submit"
                variant="contained"
                color="primary"
              >
                Login
              </Button>
            </form>
          </CardContent>
        </Card>
      </div>
    </div>
  )
}

export default Login
