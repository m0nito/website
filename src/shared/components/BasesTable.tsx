import React, { useState, useEffect } from 'react'
import axios from 'axios'
import MaterialTable from 'material-table'
import { useSnackbar } from 'notistack'

import { tableIcons } from './tableIcons'
import { ContentBox } from './ContentBox'

export const BasesTable: React.FC = () => {
  const { enqueueSnackbar } = useSnackbar()

  const [data, setData] = useState([])

  useEffect(() => {
    axios
      .get('api/bases')
      .then((res) => setData(res.data))
      .catch(() =>
        enqueueSnackbar('Can`t load data from server', {
          variant: 'error',
        })
      )
  }, [])

  return (
    <MaterialTable
      icons={tableIcons}
      columns={[
        {
          title: 'name',
          field: 'name',
        },
        {
          title: 'base id',
          field: 'baseId',
        },
        {
          title: 'AT api key',
          field: 'apiKey',
        },
        {
          title: 'created',
          field: 'created',
          editable: 'never',
          type: 'datetime',
        },
        {
          title: 'modified',
          field: 'modified',
          editable: 'never',
          type: 'datetime',
        },
      ]}
      data={data}
      title="Bases"
      editable={{
        onRowAdd: (newData) =>
          new Promise((resolve, reject) => {
            const { name, baseId, apiKey } = newData
            axios
              .post('api/bases/create', { name, baseId, apiKey })
              .then((res) => {
                setData([...data, res.data])
                enqueueSnackbar(`Successfully add base data`, {
                  variant: 'success',
                })
                resolve()
              })
              .catch((e) => {
                enqueueSnackbar('Can`t create base data', {
                  variant: 'error',
                })
                reject(e)
              })
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(async (resolve, reject) => {
            const { _id, name, baseId, apiKey } = newData
            axios
              .post('api/bases/update', {
                _id,
                fields: { name, baseId, apiKey },
              })
              .then((res) => {
                const dataUpdate = [...data]
                console.log(oldData)
                const index = oldData.tableData.id
                dataUpdate[index] = { ...newData, modified: res.data.modified }
                setData([...dataUpdate])
                enqueueSnackbar('Successfully update base data', {
                  variant: 'success',
                })
                resolve()
              })
              .catch((e) => {
                enqueueSnackbar('Can`t update base data', {
                  variant: 'error',
                })
                reject(e)
              })
          }),
        onRowDelete: (oldData) =>
          new Promise((resolve, reject) => {
            const { _id } = oldData

            axios
              .post('api/bases/delete', { _id })
              .then(() => {
                const dataDelete = [...data]
                const index = oldData.tableData.id
                dataDelete.splice(index, 1)
                setData([...dataDelete])
                enqueueSnackbar(`Successfully delete base data`, {
                  variant: 'success',
                })
                resolve()
              })
              .catch((e) => {
                enqueueSnackbar('Can`t delete base data', {
                  variant: 'error',
                })
                reject(e)
              })
          }),
      }}
      components={{
        Container: (props) => <ContentBox {...props} />,
      }}
    />
  )
}
