// for ie11
import 'core-js/es/promise'
import 'core-js/stable/object/assign'

import React from 'react'
import ReactDOM from 'react-dom'
import { loadableReady } from '@loadable/component'
import { ThemeProvider } from '@material-ui/core/styles'
import { SnackbarProvider } from 'notistack'

import { AuthProvider } from '../shared/components/AuthProvider'
import { SPTokenProvider } from '../shared/components/SPTokenProvider'

import App from '../shared/App'
import theme from '../shared/theme'

const { role } = window.__AUTH_STATE__

delete window.__AUTH_STATE__

function AppRoot(): React.FC {
  React.useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles)
    }
  }, [])

  return (
    <ThemeProvider theme={theme}>
      <SnackbarProvider maxSnack={3}>
        <AuthProvider roleProp={role}>
          <SPTokenProvider>
            <App />
          </SPTokenProvider>
        </AuthProvider>
      </SnackbarProvider>
    </ThemeProvider>
  )
}

loadableReady(() => {
  ReactDOM.hydrate(<AppRoot />, document.getElementById('root'))
})
